import keyring
import psycopg2

from Python.definitions import *
from Python.utils import *


def getProvShort(provName):
    ''' Query the postgres table to get the short prov name

    For reference to get the keyring

    for item in keyring.get_keyring().get_preferred_collection().get_all_items():
        print(item.get_label(), item.get_attributes())

    :param prov: str province name
    :return: query :str province short name (e.g.QC)
    '''

    passwd = keyring.get_password('postgresOther', 'postgres')

    try:

        conn = psycopg2.connect(
            host="localhost",
            database="census",
            user="charles",
            password=passwd)

        cur = conn.cursor()

        # execute a statement
        print(f'Querying prov name {provName}...')

        query = f'SELECT \"shortName\" ' \
                f'FROM province_names ' \
                f'WHERE name LIKE \'{provName}\''

        cur.execute(query)

        if cur.rowcount > 0:
            queryResults = str(cur.fetchone()[0]).lower()  # cur.fetchone() returns a record, which is a tuple
        else:
            raise Exception('Error with query, no records returned!')

    except Exception as e:
        print(f'Fatal error querying prov name {provName}'
              f'\nError: {e}')

    finally:
        if conn is not None:
            conn.close()

    if len(queryResults) != 2:
        raise Exception(f'Fatal error! the prvince short names must have 2 characters , not {len(queryResults)}')

    return queryResults




def checkTableExistence(tblName):


    passwd = keyring.get_password('postgresOther', 'postgres')

    try:

        conn = psycopg2.connect(
            host="localhost",
            database="ms_buildings",
            user="charles",
            password=passwd)

        cur = conn.cursor()

        # execute a statement
        print(f'Checking if table {tblName} exists...')

        query = f'SELECT ' \
                f'EXISTS( ' \
                f'    SELECT ' \
                f'FROM ' \
                f'information_schema.tables ' \
                f'WHERE ' \
                f'       table_schema = \'public\' ' \
                f'AND    table_name   = \'{tblName}\' );'

        cur.execute(query)

        if cur.rowcount > 0:
            queryResults = bool(cur.fetchone()[0])  # cur.fetchone() returns a record, which is a tuple
        else:
            raise Exception('Error with query, no records returned!')

    except Exception as e:
        print(f'Fatal error querying prov name {tblName}'
              f'\nError: {e}')

    finally:
        if conn is not None:
            conn.close()

    return queryResults




def getProvinceTerritoriesNames():


    passwd = keyring.get_password('postgresOther', 'postgres')

    try:

        conn = psycopg2.connect(
            host="localhost",
            database="census",
            user="charles",
            password=passwd)

        cur = conn.cursor()



        query = 'SELECT name ' \
                'FROM \"pr_c_Canada\" ;'

        cur.execute(query)

        if cur.rowcount > 0:
            queryResults = cur.fetchall() # list of tuples
        else:
            raise Exception('Error with query, no records returned!')

    except Exception as e:
        print(f'Fatal error querying the table with the list of provinces'
              f'\nError: {e}')

    finally:
        if conn is not None:
            conn.close()

    listProvinces = [ record[0] for record in queryResults]

    #13 provinces and territories
    assert len(listProvinces) == 13

    return listProvinces