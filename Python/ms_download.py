
import os
import requests
import zipfile
import shutil


from Python.definitions import *
from Python.utils import *
from Python.postgresQueries import *


def downloadMSBuildingsByProv(provinceSubset=None):

    #Paths to directories
    listProv  = listProv  = getProvinceTerritoriesNames()
    basePathJSON = ROOT_DIR / 'Data'  / "MsBuildingsJSON"

    #Url for ms buildings dataset
    baseUrl = "https://usbuildingdata.blob.core.windows.net/canadian-buildings-v2"


    #Subset prov
    if provinceSubset is not None:
        listProv=[ p for p in listProv if p in provinceSubset ]
        print(f'In downloadMSBuildingsByProv: considering the following province subset: {", ".join(listProv)}')

    for p in listProv:

        #Paths
        pathFileGeojson  =basePathJSON / p / f'{p}.geojson'
        pathFileZip  =basePathJSON / p / f'{p}.zip'

        # String manips required to get all provinces and territories
        pMod = convertProvName(p)

        # Download
        ## All done
        if  os.path.isfile(pathFileGeojson)  :
            print(f'File {pathFileGeojson} has already been downloaded')

        ## No zip or geojson file - need to download
        elif not os.path.isfile(pathFileZip):

            #Remove empty directory if it exists to avoid collisions
            if os.path.isdir(basePathJSON / p):
                shutil.rmtree(basePathJSON / p)
            #Make sure it exists
            createDirIfNotExists(basePathJSON / p)

            print(f'Downloading {pathFileZip} ... ')

            requestResult=requests.get(f'{baseUrl}/{pMod}.zip',verify=False,stream=True)
            if requestResult.status_code == 200:

                with open(pathFileZip, 'wb+') as f:
                    f.write(requestResult.content)
            else:
                raise Exception(f'Fatal error downloading {pathFileZip} -\nStatus code : {requestResult.status_code}')


        #Unzip - could have been downloaded manually
        if not os.path.isfile(pathFileGeojson) and \
            os.path.isfile(pathFileZip):
            print('Unzipping')
            try:
                with zipfile.ZipFile(pathFileZip, 'r') as zip_ref:
                    zip_ref.extractall(str(basePathJSON / p))
            except :
                print('Fatal error unzipping province {p} - {e}')


        #Weird fuck up with name - make sure the geojson has spaces and corresponds to the official province name used by the census
        if os.path.isfile(basePathJSON / p / f'{pMod}.geojson'):
            os.rename(str(basePathJSON / p / f'{pMod}.geojson'), str(pathFileGeojson))

        #Remove the zip file
        if os.path.isfile(pathFileZip):
            os.remove(pathFileZip)


    print('Successfully finished downloading geojsons...')


