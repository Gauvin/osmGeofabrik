
import subprocess
import keyring


from Python.definitions import *
from Python.postgresQueries import *




def convertMsBuildingsGeojsonPostgis(provinceSubset=None):

    print('Converting the geojson to postgis tables')

    #Paths to directories
    listProv  = getProvinceTerritoriesNames()
    basePathJSON = ROOT_DIR / 'Data'  / "MsBuildingsJSON"
    basePathBuildings = ROOT_DIR / 'Data' / "MsBuildings"

    #Subset prov
    if provinceSubset is not None:
        listProv=[ p for p in listProv if p in provinceSubset ]
        print(f'In convertMsBuildingsGeojsonPostgis: converting the following province subset: {", ".join(listProv)}')

    for p in listProv:

        #Paths
        listJson = [ f for f in os.listdir(basePathJSON / p  ) if f.endswith(".geojson") ]

        if len(listJson) == 0 :
            raise Exception(f'Fatal error! no geojson for {p}')
        elif len(listJson) > 1:
            print(f'Warning! there are {len(listJson)} json files! Considering first one')


        jsonFile = listJson[0]
        shpFile = basePathBuildings / f'MsBuildings{p}' / f'MsBuildings{p}.shp'

        createDirIfNotExists(basePathBuildings / f'MsBuildings{p}')

        if os.path.isfile( shpFile ):
            print(f'File {shpFile} has already been converted')
        else:
            print(f'Converting Geojson to shp for {p}')

            passwd = keyring.get_password('postgresOther', 'postgres')
            shortName = getProvShort(p)


            tableExists = checkTableExistence(f'pr_{shortName}_buildings')

            if tableExists:
                print(f'Table {tableExists} already exists..')
            else:
                print('Running ')

                #First get the path script
                pathScript = subprocess.check_output(['which', 'writeGeojsonToPostgres.sh' ]   ).strip().decode("utf-8")
                pathGeoJsplitScript = subprocess.check_output(['which', 'geojsplit']).strip().decode("utf-8")

                if pathScript is None or len(pathScript) == 0:
                    raise Exception('Fatal error getting the script path!')
                if pathGeoJsplitScript is None or len(pathGeoJsplitScript) == 0:

                    raise Exception('Fatal error getting the geojsplit path!')

                #Move to the correct directory
                os.chdir(str(basePathJSON / p ))

                cmd = " ".join(
                    [ pathScript,
                     'ms_buildings',
                     f'pr_{shortName}_buildings',
                     f'\'{str(jsonFile)}\'',  # quote the name
                     passwd,
                     '4326',
                     '4326'
                     ]
                )

                #subprocess seems to always fail
                #make sure sudo pip3 install geojsplit
                result = os.system(cmd)

                if result != 0:
                    print(f'Warning! the geojsplit returned {result} for province{p}')

