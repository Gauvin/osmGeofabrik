
from pathlib import Path
import re
import os




def createDirIfNotExists( subDirPath : Path) -> Path:

    if not os.path.isdir(subDirPath):
        os.makedirs(subDirPath)  # recursive

    assert os.path.isdir(subDirPath)

    return Path(subDirPath)


def grepAllFilesDeleteInDir( filePath : Path) :

    '''Find all files that match a string in a dir and delete them

    #For instance given /home/charles/FloodMaps/Data/MsBuildingsDensity/MsBuildingsDensityAlberta/MsBuildings_densityAlberta_200_500.tif
    #delete:

    MsBuildings_densityAlberta_200_500.tif
    MsBuildings_densityAlberta_200_500.tfw
    MsBuildings_densityAlberta_200_500.*

    etc.
    '''

    #Check the file exists - this might not be an error, don't throw an exception
    #Still continue running:
    #   Possible that MsBuildings_density_ecumen_Alberta_200_500.tif was removed, but
    #   MsBuildings_density_ecumen_Alberta_200_500.xml remains (and should be deleted)
    if not os.path.isfile(filePath):
        print (f'Warning when grepping files - {filePath} does not exists\nIt might have already been deleted!')


    #Get ALL files in the parent directory
    listFiles=os.listdir(filePath.parent)

    #Get the stem - e.g. MsBuildings_densityAlberta_200_500 in MsBuildings_densityAlberta_200_500.tif
    #   Match MsBuildings_densityAlberta_200_500
    #   But NOT MsBuildings_densityAlberta_200_500_subset.tif
    #   nor prefix_MsBuildings_densityAlberta_200_500.tif
    pattern =re.compile(f'^{filePath.stem}[.].*')

    #Get all matching file names
    #findall returns a list for each element so listFilesToDelete is a list of lists
    listFilesToDelete = [ pattern.findall(s) for s in listFiles]

    #Remove them
    [os.remove( str(filePath.parent / f[0]) ) for f in listFilesToDelete if len(f) > 0 ]


    return True

def convertProvName(p):


    pMod = str(p)  # Remove white space
    pMod = pMod.title()  # NewfoundlandandLabrador -> NewfoundlandAndLabrador
    pMod = pMod.replace(" ", "") #no spaces

    # Special case for Yukon (very weird)
    pMod = f'{pMod}Territory' if pMod == 'Yukon' else pMod

    return pMod