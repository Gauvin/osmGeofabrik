
# Introduction

Project to analyse Canadian city ecumenes. 

- Downloads and writes the Microsoft buildings to a postgis db in Python and bash
- Computes 2D raster KDE in R 
- Computes polygonized rasters to identify the ecumen (built areas)


# Data sources

- Microsoft canadian buildings open database
- Natural ressource Canada (NRCAN), namely compiling using some Lidar data from Montreal cities and others
- OSM data (packaged from geofabrik)
- Stats can buildings open database 
